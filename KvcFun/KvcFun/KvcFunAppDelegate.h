//
//  KvcFunAppDelegate.h
//  KvcFun
//
//  Created by Andrew Burger on 5/19/13.
//  Copyright (c) 2013 Andrew Burger. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface KvcFunAppDelegate : NSObject <NSApplicationDelegate> {
    int fido;
}

@property (assign) IBOutlet NSWindow *window;
@property (readwrite, assign) int fido;

- (IBAction)incrementFido:(id)sender;


@end
