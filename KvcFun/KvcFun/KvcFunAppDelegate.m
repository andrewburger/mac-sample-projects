//
//  KvcFunAppDelegate.m
//  KvcFun
//
//  Created by Andrew Burger on 5/19/13.
//  Copyright (c) 2013 Andrew Burger. All rights reserved.
//

#import "KvcFunAppDelegate.h"

@implementation KvcFunAppDelegate

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification
{
    // Insert code here to initialize your application
}

- (id)init {
    self = [super init];
    if (self) {
        [self setValue:[NSNumber numberWithInt:5] forKey:@"fido"];
        NSNumber *n = [self valueForKey:@"fido"];
        NSLog(@"fido = %@", n);
    }
    return self;
}

@synthesize fido;

- (IBAction)incrementFido:(id)sender {
    // --- More expansive version --- //
    //[self willChangeValueForKey:@"fido"];
    //fido++;
    //NSLog(@"fido is now %d", fido);
    //[self didChangeValueForKey:@"fido"];
    [self setFido:[self fido] + 1];
}

@end
