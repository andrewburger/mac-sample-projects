//
//  RandomController.h
//  Random
//
//  Created by Andrew Burger on 5/13/13.
//  Copyright (c) 2013 Andrew Burger. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RandomController : NSObject {
    IBOutlet NSTextField *textField;
}

- (IBAction)seed:(id)sender;
- (IBAction)generate:(id)sender;

@end
