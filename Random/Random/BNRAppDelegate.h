//
//  BNRAppDelegate.h
//  Random
//
//  Created by Andrew Burger on 5/10/13.
//  Copyright (c) 2013 Andrew Burger. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface BNRAppDelegate : NSObject <NSApplicationDelegate>

@property (assign) IBOutlet NSWindow *window;

@end
