//
//  RandomController.m
//  Random
//
//  Created by Andrew Burger on 5/13/13.
//  Copyright (c) 2013 Andrew Burger. All rights reserved.
//

#import "RandomController.h"

@implementation RandomController

- (IBAction)generate:(id)sender {
    // Generate a number between 1 and 100 inclusive
    int generated;
    generated = (int)(random() % 100) + 1;
    
    NSLog(@"generated = %d", generated);
    
    // Ask the text field to change what it is displaying
    [textField setIntValue:generated];
}

- (IBAction)seed:(id)sender {
    // Seed the random number generator
    srandom((unsigned) time(NULL));
    [textField setStringValue:@"Generator seeded"];
}

- (void)awakeFromNib {
    NSDate *now;
    now = [NSDate date];
    [textField setObjectValue:now];
}

@end
