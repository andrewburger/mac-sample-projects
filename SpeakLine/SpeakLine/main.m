//
//  main.m
//  SpeakLine
//
//  Created by Andrew Burger on 5/15/13.
//  Copyright (c) 2013 Andrew Burger. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, char *argv[])
{
    return NSApplicationMain(argc, (const char **)argv);
}
