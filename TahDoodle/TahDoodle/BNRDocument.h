//
//  BNRDocument.h
//  TahDoodle
//
//  Created by Andrew Burger on 4/27/13.
//  Copyright (c) 2013 Andrew Burger. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface BNRDocument : NSDocument <NSTableViewDataSource> {
    NSMutableArray *todoItems;
    IBOutlet NSTableView *itemTableView;
}

- (IBAction)createNewItem:(id)sender;

- (IBAction)deleteSelectedItem:(id)sender;

@end
