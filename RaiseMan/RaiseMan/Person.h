//
//  Person.h
//  RaiseMan
//
//  Created by Adam Preble on 9/19/11.
//  Copyright (c) 2011 Big Nerd Ranch. All rights reserved.
//
//  Source modified by Andrew Burger on 6/12/13
//

#import <Foundation/Foundation.h>

@interface Person : NSObject <NSCoding> {
	NSString *personName;
	float expectedRaise;
}
@property (readwrite, copy) NSString *personName;
@property (readwrite) float expectedRaise;

@end
