//
//  PreferenceController.m
//  RaiseMan
//
//  Created by Andrew Burger on 6/13/13.
//  Copyright (c) 2013 Big Nerd Ranch. All rights reserved.
//

#import "PreferenceController.h"

NSString * const BNRTableBgColorKey = @"BNRTableBackgroundColor";
NSString * const BNREmptyDocKey = @"BNREmptyDocumentFlag";
NSString * const BNRColorChangedNotification = @"BNRColorChanged";

@interface PreferenceController ()

@end

@implementation PreferenceController

- (id) init {
    self = [super initWithWindowNibName:@"Preferences"];
    return self;
}

- (void)windowDidLoad
{
    [super windowDidLoad];
    
    [colorWell setColor:[PreferenceController preferenceTableBgColor]];
    [checkBox setState:[PreferenceController preferenceEmptyDoc]];
    
    // Implement this method to handle any initialization after your window controller's window has been loaded from its nib file.
    NSLog(@"Nib file is loaded");
}

- (IBAction)changeBackgroundColor:(id)sender {
    NSColor *color = [colorWell color];
    NSLog(@"Color changed: %@", color);
    [PreferenceController setPreferenceTableBgColor:color];
    
    NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
    NSLog(@"Sending notification");
    [nc postNotificationName:BNRColorChangedNotification object:self];
    
    NSDictionary *d = [NSDictionary dictionaryWithObject:color forKey:@"color"];
    [nc postNotificationName:BNRColorChangedNotification
                      object:self
                    userInfo:d];
}

- (IBAction)changeNewEmptyDoc:(id)sender {
    NSInteger state = [checkBox state];
    NSLog(@"Checkbox changed %ld", state);
    [PreferenceController setPreferenceEmptyDoc:state];
}

+ (NSColor *)preferenceTableBgColor {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSData *colorAsData = [defaults objectForKey:BNRTableBgColorKey];
    return [NSKeyedUnarchiver unarchiveObjectWithData:colorAsData];
}

+ (void)setPreferenceTableBgColor:(NSColor *)color {
    NSData *colorAsData = [NSKeyedArchiver archivedDataWithRootObject:color];
    [[NSUserDefaults standardUserDefaults] setObject:colorAsData forKey:BNRTableBgColorKey];
}

+ (BOOL)preferenceEmptyDoc {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    return [defaults boolForKey:BNREmptyDocKey];
}

+ (void)setPreferenceEmptyDoc:(BOOL)emptyDoc {
    [[NSUserDefaults standardUserDefaults] setBool:emptyDoc forKey:BNREmptyDocKey];
}

- (IBAction)resetPreferences:(id)sender {
    NSLog(@"Resetting Defaults");
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:BNREmptyDocKey];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:BNRTableBgColorKey];
    [self windowDidLoad];
}

/*
 *
 ***********************************
 *** Original code block ignored ***
 ***********************************
 *
 
- (id)initWithWindow:(NSWindow *)window
{
    self = [super initWithWindow:window];
    if (self) {
        // Initialization code here.
    }
    
    return self;
}
 */



@end
