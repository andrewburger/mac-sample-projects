//
//  AppController.h
//  RaiseMan
//
//  Created by Andrew Burger on 6/13/13.
//  Copyright (c) 2013 Big Nerd Ranch. All rights reserved.
//


#import <Foundation/Foundation.h>

@class PreferenceController;

@interface AppController : NSObject {
    PreferenceController *preferenceController;
}

-(IBAction)showPreferencePanel:(id)sender;
-(IBAction)showAboutPanel:(id)sender;

@end
