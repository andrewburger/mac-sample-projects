//
//  BigLetterView.h
//  TypingTutor
//
//  Created by Andrew Burger on 7/24/13.
//  Copyright (c) 2013 Andrew Burger. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface BigLetterView : NSView {
    NSColor *bgColor;
    NSString *string;
    NSMutableDictionary *attributes;
    BOOL bold;
    BOOL italic;
    NSEvent *mouseDownEvent;
    BOOL highlighted;
}

@property (strong) NSColor *bgColor;
@property (copy) NSString *string;
@property (nonatomic) BOOL bold;
@property (nonatomic) BOOL italic;

- (void)prepareAttributes;
- (IBAction)savePDF:(id)sender;
- (IBAction)toggleBold:(NSButton *)sender;
- (IBAction)toggleItalic:(NSButton *)sender;
- (IBAction)cut:(id)sender;
- (IBAction)copy:(id)sender;
- (IBAction)paste:(id)sender;

@end
