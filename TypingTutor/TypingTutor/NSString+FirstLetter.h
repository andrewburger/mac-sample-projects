//
//  NSString+FirstLetter.h
//  TypingTutor
//
//  Created by Andrew Burger on 8/6/13.
//  Copyright (c) 2013 Andrew Burger. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (FirstLetter)

- (NSString *)bnr_firstLetter;

@end
