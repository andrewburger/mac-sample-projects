//
//  NSString+FirstLetter.m
//  TypingTutor
//
//  Created by Andrew Burger on 8/6/13.
//  Copyright (c) 2013 Andrew Burger. All rights reserved.
//

#import "NSString+FirstLetter.h"

@implementation NSString (FirstLetter)

- (NSString *)bnr_firstLetter {
    if ([self length] < 2) {
        return self;
    }
    NSRange r;
    r.location = 0;
    r.length = 1;
    return [self substringWithRange:r];
}

@end
